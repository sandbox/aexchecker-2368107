<?php

/**
 * @file
 * Define a MigrateSource for importing BLM file.
 * Parser for Rightmove's lovely BLM files.
 */

/**
 * Implementation of MigrateSource, to handle imports from BLM files.
 */
class MigrateSourceBLM extends MigrateSource {
  /**
   * BLM Data.
   *
   * @var string
   */
  protected $blm;

  /**
   * Header of BLM data.
   * @var array
   */
  protected $header = array();

  /**
   * Definitions
   *
   * @var array
   */
  protected $def = array();

  /**
   * Structured data rows.
   *
   * @var array
   */
  protected $data = array();

  /**
   * The current row/line number in the BLM file.
   *
   * @var integer
   */
  protected $currentId;

  /**
   * Row/line numbers in the BLM file.
   *
   * @var integer
   */
  protected $numRows;

  /**
   * Initialization.
   *
   * @param string $path
   *          The path to the source file
   * @param array $options
   *          Options applied to this source.
   */
  public function __construct($path, array $options = array()) {
    parent::__construct($options);
    $this->file = $path;
    $this->blm = file_get_contents($this->file);
    $this->splitPieces();
    $this->numRows = $this->propCount();
  }

  /**
   * Return a string representing the source, for display in the UI.
   */
  public function __toString() {
    return t('MigrateSourceBLM: Generate "%num" sample rows from "%file" file.', array(
      '%num' => $this->numRows,
      '%file' => $this->file
    ));
  }

  /**
   * Returns a list of fields available to be mapped from the source,
   * keyed by field name.
   */
  public function fields() {
    return $this->header;
  }

  /**
   * Return the number of available source records.
   */
  public function computeCount() {
    return $this->numRows;
  }

  /**
   * Needs to be done to start a fresh traversal of the source data.
   */
  public function performRewind() {
    $this->currentId = 0;
  }

  /**
   * Fetch the next row of data, returning it as an object.
   * Return FALSE when there is no more data available.
   */
  public function getNextRow() {
    if ($this->currentId <= $this->numRows) {
      $row = (object) $this->data[$this->currentId];
      $row->id = $this->currentId;
      $this->currentId ++;
      return $row;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns specific field from row.
   *
   * @param string $data
   * @param int $row
   *
   * @return array
   */
  public function getData($data, $row) {
    return $this->data[$row][$data];
  }

  /**
   * Return header info.
   *
   * @param string $hdr
   *
   * @return string
   */
  public function getHeader($hdr) {
    return $this->header[$hdr];
  }

  /**
   * This will return the actual number of properties, regardless of
   * what the header might say.
   *
   * @return number
   */
  public function propCount() {
    return count($this->data);
  }

  public function properties() {
    return $this->data;
  }

  /**
   * Splits the BLM data into constituent parts.
   */
  protected function splitPieces() {
    $pieces = explode("#", $this->blm);

    // Get the header (includes EOF/EOR stuff)
    $header = explode("\n", trim($pieces[2]));
    foreach ($header as $h) {
      // Remove quotes on EOF/EOR
      $h = preg_replace("/\'/", "", $h);
      $h_pieces = explode(" : ", $h);
      $this->header[trim($h_pieces[0])] = trim($h_pieces[1]);
    }

    // Get the definitions
    $def = explode($this->header['EOF'], trim($pieces[4]));
    foreach ($def as $d) {
      $this->def[] = $d;
    }

    // Get the data
    $data = explode($this->header['EOR'], trim($pieces[6]));
    $datac = count($data);
    for ($i = 0; $i < $datac; $i ++) {
      $row = explode($this->header['EOF'], trim($data[$i]));
      for ($j = 0; $j < count($row); $j ++) {
        $row_arr[$this->def[$j]] = $row[$j];
      }
      $this->data[] = $row_arr;
    }
  }
}

