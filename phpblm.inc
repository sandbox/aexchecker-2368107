<?php

/**
 * Parser for Rightmove's lovely BLM files.
 */
class phpblm {
  protected $blm;
  protected $header = array();
  protected $def = array();
  protected $data = array();

  public function __construct($file) {
    $this->blm = file_get_contents($file);
    $this->splitPieces();
  }

  /**
   * Return specific field from row.
   *
   * @param string $data
   * @param int $row
   *
   * @return array
   */
  public function getData($data, $row) {
    return ! empty($this->data[$row][$data]) ? $this->data[$row][$data] : FALSE;
  }

  /**
   * Return specific row.
   *
   * @param int $row
   *
   * @return array
   */
  public function getRow($row) {
    return ! empty($this->data[$row]) ? $this->data[$row] : FALSE;
  }

  /**
   * Return header info.
   *
   * @param string $hdr
   *
   * @return string
   */
  public function getHeader($hdr) {
    return $this->header[$hdr];
  }

  /**
   * This will return the actual number of properties, regardless of what the
   * header might say. To retrieve the header value (if present), use:
   * $blm->getHeader('Property Count');
   *
   * @return int
   */
  public function propCount() {
    static $count;

    if (!isset($count)) {
      if ($this->getHeader('Property Count')) {
        $count = $this->getHeader('Property Count');
      }
      else {
        $count = count($this->data);
      }
    }

    return $count;
  }

  public function properties() {
    return $this->data;
  }

  /**
   * Splits the BLM data into constituent parts.
   */
  protected function splitPieces() {
    $pieces = explode("#", $this->blm);

    // Get the header (includes EOF/EOR stuff)
    $header = explode("\n", trim($pieces[2]));
    foreach ($header as $h) {
      // Remove quotes on EOF/EOR
      $h = preg_replace("/\'/", "", $h);
      $h_pieces = explode(" : ", $h);
      $this->header[trim($h_pieces[0])] = trim($h_pieces[1]);
    }

    // Get the definitions
    $def = explode($this->header['EOF'], trim($pieces[4]));
    foreach ($def as $d) {
      $this->def[] = $d;
    }

    // Get the data
    $data = explode($this->header['EOR'], trim($pieces[6]));
    $datac = count($data);

    for ($i = 0; $i < $datac; $i ++) {
      $row = explode($this->header['EOF'], trim($data[$i]));
      for ($j = 0; $j < count($row); $j ++) {
        $row_arr[$this->def[$j]] = $row[$j];
      }
      $this->data[] = $row_arr;
    }
  }
}

