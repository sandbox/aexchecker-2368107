<?php
/**
 * @file
 * migrate_sources_blm hook implementation,
 * needs to register migrate_sources_blm.inc in migration module.
 */


/*
 * Implements hook_migrate_api().
 */
function migrate_sources_blm_migrate_api() {
  return array(
    'api' => MIGRATE_API_VERSION,
  );
}

